;;We need to:
;;Once the inline button is clicked we need to:
;;Find all the CSS files in the specified directory...
;;We then need to extract all image paths from a css file
;;With each of the paths, we need to find the image in the supplied
;;image path
;;Load and convert the image into a base64 format
;;Then replace the image url with the base64
;;After all of that is done its time to move on to the next file

(ns inliner1.core
  (:use [inliner1.frame]
        [inliner1.css]
        [clojure.pprint])
  (:gen-class))

(defn inline
  [params]
  (let [write (params :write-status)
        path  (params :css-path)]

    (pprint
     (process-path path write)
     )
    ))

(defn -main
  []
  (make-frame inline)
  0)




