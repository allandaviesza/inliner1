;;How should this program work then?
;;
;;

(ns inliner1.core
  (:use [seesaw.core]
        [clojure.pprint])
  (:require [clojure.data.codec.base64 :as b64]
            [clojure.java.io :as io])
  (:import [java.io File]))

(defn base64-encode [original]
  (String. (b64/encode original)))

(defn slurp-binary-file! [path]
  (let [file (io/file path)]
    (io! (with-open [reader (io/input-stream file)]
         (let [buffer (byte-array (.length file))]
           (.read reader buffer)
           buffer)))))

(defn encode-image
  []
  (str "data:image/png;base64,"
       (base64-encode
        (slurp-binary-file! "text/img.png"))))

(defn get-content
  []
  (grid-panel
   :border [5 "Settings" 10]
   :columns 3
   :vgap 10
   :items ["CSS Folder"
           (text :columns 30 :text "c:\\clojure-workspace\\inliner1")
           (button :text "Browse" :size [100 :by 100])
           "Image Folder" (text :columns 30)
           (button :text "Browse")
           (button :text "Inline!")]))

(defn find-urls
  [css]
  (re-seq #"url\('(.*)'\)" css))

(defn replace-urls
  [css found-urls]
  (let [url (first (rest (first found-urls)))
        replaced (clojure.string/replace css url (encode-image))
        nextit (rest found-urls)]
    (if (empty? nextit)
      css
      (recur replaced nextit))))

(defn -main
  []
  (native!)
  (-> (frame ;;:on-close :exit
             :title "Inliner1"
             :content (get-content)) pack! show!)
  (let [css (slurp "text/test.html")
        urls (find-urls css)
        replaced (replace-urls css urls)]
    (spit "text/test_done.html" replaced))
  (encode-image))




