(ns inliner1.image
  (:use [clojure.pprint])
  (:require [clojure.data.codec.base64 :as b64]
            [clojure.java.io :as io])
  (:import [java.io File]))

(defn base64-encode [original]
  (String. (b64/encode original)))

(defn slurp-binary-file! [path]
  (let [file (io/file path)]
    (io! (with-open [reader (io/input-stream file)]
           (let [buffer (byte-array (.length file))]
             (.read reader buffer)
             buffer)))))

(defn encode
  "Load an image from the supplied path,
  then base 64 encode and return the image"
  [path]
  (let [length (.length path)
        ext (subs path (- length 3) length)]
    (str "data:image/" ext ";base64,"
      (base64-encode
        (slurp-binary-file! path)))))