(ns inliner1.css
  (:use clojure.pprint)
  (:require [inliner1.image :as image]
            [clojure.java.io :as io]
            [clojure.string  :as string]))

(defn search [s] (peek (re-find #"background:\s*url\(['\"]*(.+?)['\"]*\)" s)))

(defn embed-image
  "
    Replace the url with an encoded image
  "
  [line url base-url]
  (string/replace line url (str "\"" (image/encode (str base-url "/" url)) "\"")))

(defn find-and-process-url
  "
    Given a file, search for occurances of url's pointing
    to images. Apply the process-fn to process the found url.

    The process-fn is passed the line the url was found in
    and the url itself.
  "
  [file process-fn]
  (with-open [rdr (io/reader file)]
    (reduce (fn [s l]
              (let [res (search l)]
                (if (not= res nil)
                  (str s (process-fn l res) "\n")
                  (str s l "\n"))))
            ""
            (line-seq rdr))))

(defn get-file-seq
  "
    Return a sequence of files from the passed path,
    filtered by the passed string.
  "
  [path filter-str]
  (let [dir (io/file path)]
    (filter (fn [f] (not= (re-find #"^[^\.]*\.(css)$" (.getName f)) nil))
            (file-seq dir))))

(defn process-path
  "
    Find all css files in the path. Search the files for
    urls to images. Replace the urls with their respective images.
  "
  [path write-status]
  (let [files (get-file-seq path "css")
        process (fn [line url] (embed-image line url path))]
    (map (fn
           [file]
           (write-status (str "Processing: " (.getName file)))
           (spit
            (string/replace (.getCanonicalPath file) ".css" ".in.css")
            (find-and-process-url file process))) files)))