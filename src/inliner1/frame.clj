(ns inliner1.frame
  (:use [seesaw.core]
        [seesaw.chooser]
        [clojure.pprint])
  (:require [clojure.string :as string :only [blank?]]))

(defn write-status
  [f status]
  (let [t (select f [:#status])]
    (config! t :text (str (config t :text) status "\n"))))

(defn browse-files
  []
  (let [file (choose-file :selection-mode :dirs-only)]
    (if file (.getPath file) "")))

(defn get-browse-action
  [text-widget]
  (action :handler (fn [e] (config! text-widget :text (browse-files)))
          :name "Browse"))

(defn get-inline-action
  [f css-path inline]
  (action
   :name "Inline"
   :handler (fn [e]
              (let [css-path (config css-path :text)]
                (if (string/blank? css-path)
                  (write-status f "CSS path missing.")
                  (inline {:write-status (fn [s] (write-status f s))
                           :css-path css-path}))))))


(defn add-actions
  [f inline-fn]
  (let [css-button (select f [:#css-button])
        css-path (select f [:#css-path])
        inline (select f [:#inline!])]
    (config! css-button :action (get-browse-action css-path))
    (config! inline :action (get-inline-action f css-path inline-fn)))
  f)

(defn get-button-panel
  [buttons]
  (flow-panel
    :vgap 5
    :hgap 5
    :align :left
    :items buttons))

(defn get-css-buttons
  []
  (get-button-panel [(label  :text "CSS Folder" :size [100 :by 20])
                     (text   :id :css-path :columns 30 :text "C:\\clojure-workspace\\inliner1\\test\\sandbox\\template\\css")
                     (button :id :css-button)]))

(defn get-content
  []
  (border-panel
   :north (grid-panel
           :columns 1
           :items [(get-css-buttons)
                   (get-button-panel [(button :id :inline!)])])
   :south (text :id :status
                :multi-line? true :editable? false
                :wrap-lines? true :rows 5)))

(defn init-frame
  []
  (frame :on-close :exit
         :title "Inliner1"
         :icon (seesaw.icon/icon "icon.png")
         :content (get-content)))

(defn make-frame
  [inline-fn]
  (native!)
  (let [f (init-frame)]
    (-> (add-actions f inline-fn) pack! show!)))