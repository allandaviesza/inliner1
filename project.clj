(defproject inliner1 "1.0.1"
  :description "A simple Clojure program to inline images files in to CSS files."
  :url "https://bitbucket.org/allandaviesza/inliner1"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [seesaw "1.4.3"]
                 [org.clojure/data.codec "0.1.0"]
                 [org.clojure/clojure-contrib "1.2.0"]]
  :main inliner1.core)
