# Inliner1

A simple Clojure program to inline images files in to CSS files.

## Usage

Double Click.

## License

Copyright © 2013 Allan Davies

Distributed under the Eclipse Public License, the same as Clojure.
