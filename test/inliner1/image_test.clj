(ns inliner1.image-test
  (:require [clojure.test :refer :all]
            [inliner1.image :refer :all]))

(def PATH
  "c:\\clojure-workspace\\inliner1\\test\\sandbox\\template\\images\\extra-banner.png")
(def FUNNYPATH
  "c:\\clojure-workspace\\inliner1\\test\\sandbox\\template/images/extra-banner.png")
(def FUNNYPATH2
  "c:\\clojure-workspace\\inliner1\\test\\sandbox\\template\\css/../images/extra-banner.png")

(deftest slurp-binary-test
  (testing "slurp-binary-file! tests"
    (testing "Does slurp return the expected number of bytes?"
      (is (=
           (count (slurp-binary-file! PATH))
           23667)))
    (testing "How does slurp handle funny slashes in the path?"
      (is (=
           (count (slurp-binary-file! FUNNYPATH))
           23667)))
    (testing "Can we move up a directory?"
      (is (=
           (count (slurp-binary-file! FUNNYPATH2))
           23667)))))

(deftest encode-test
  (testing "encode tests > "
    (testing "is the mime type set correctly?"
      (is (=
           (subs (encode FUNNYPATH2) 11 14)
           "png")))))

