(ns inliner1.css-test
  (:require [clojure.test :refer :all]
            [inliner1.css :refer :all]
            [clojure.pprint :refer :all]))

(def path "c:\\clojure-workspace\\inliner1\\test\\sandbox\\template\\styles")
(def path-css "c:\\clojure-workspace\\inliner1\\test\\sandbox\\template\\css")
(def fnp-path "c:\\clojure-workspace\\inliner1\\test\\sandbox\\find-and-process.css")

(deftest test-get-file-seq
  (testing "Does get-file-seq actually return a lazy seq?"
    (is (=
         (type (get-file-seq path "css"))
         clojure.lang.LazySeq))))

(deftest get-file-seq-count-test
  (testing "Do we get the correct numbers of items in the seq?"
    (is (=
         (count (get-file-seq path "css"))
         3))))

(deftest get-file-seq-count2-test
  (testing "When the dir is called css, do we get the correct count?"
    (is (=
         (count (get-file-seq path-css "css"))
         3))))

(deftest embed-image-test
  (testing "does the url get replaced with what we expect?"
    (is (=
         (count (embed-image "	background:url(../images/header-pattern.gif) 50% 0 #1d1d1d;"
                 "../images/header-pattern.gif"
                 "C:\\clojure-workspace\\inliner1\\test\\sandbox\\template\\css"))
         216))))

(deftest find-and-process-test
 (testing "there should be newlines in the output"
    (is (=
         (re-find #"\n"
                  (find-and-process-url fnp-path (fn [line url] line)))
         "\n"))))

(deftest search-test
  (testing "url search function > "
    (testing "paths found with no quotes?"
      (is (=
           (search "background:url(../images/header-pattern.gif) 50% 0 #1d1d1d;")
           "../images/header-pattern.gif")))

    (testing "path found with single quotes?"
      (is (=
           (search "background:url('../images/header-pattern.gif') 50% 0 #1d1d1d;")
           "../images/header-pattern.gif")))

    (testing "path found with double quotes?"
      (is (=
           (search "background:url(\"../images/header-pattern.gif\") 50% 0 #1d1d1d;")
           "../images/header-pattern.gif")))

    (testing "path should not be found when not background"
      (is (=
           (search "behaviour:url(../images/header-pattern.gif);")
           nil)))

     (testing "path found, space between : and url?"
      (is (=
           (search "background: url(../images/header-pattern.gif) 50% 0 #1d1d1d;")
           "../images/header-pattern.gif")))
    ))